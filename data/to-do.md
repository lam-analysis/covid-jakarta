TASK:
- Clean the date: onset, arrival, discharge, lab
- Use NIK_clean to fill NA date of birth dan age
- Subgroup analysis based on date of onset -> Different symptoms?
- Combine clustering with PCA -> A few symptoms are strong mortality predictor
- Multilevel analysis -> SES and commuting data (use machine learning)
- Symptoms seasonality

DONE:
- Formalize field's name
- Merge from updated files
- Remove duplicates based on:
  - Nama
  - Tanggal Lahir
  - Tanggal Hasil Lab
  - NIK
  - Umur
  - Alamat
- Separate HCW before cleaning the name
  - dr., drg., ns., ners nz., zr., dokter, doctor
- Clean the NIK
- Clean the address
- Fill NA
- Create a metadata (e.g.: MIBBI, Dublin core, etc.)
