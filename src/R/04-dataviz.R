## LOAD PACKAGES

pkgs <- c("magrittr", "ggplot2", "sjPlot")
pkgs.load <- sapply(pkgs, library, character.only=TRUE)


## PREP

# Load the required objects
load("RData/02-glmer.RData")


## PLOT

# Mixed effect report
plt.glmer.swab <- plot_model(
    glmer.swab, type="re", facet.grid=TRUE, axis.lim=c(0.6, 2), ci.lvl=0.95,
    show.p=TRUE, show.values=TRUE, show.data=TRUE, dot.size=2.5, line.size=1,
    value.size=4, p.threshold=0.05, p.val="wald", colors=c("grey60", "grey20"),
    vline.color="grey70"
) +
    labs(
        title    = "Monthly variance of the odds ratio in having positive swab results",
        subtitle = "Month of data collection as a random effect"
    ) +
    ggpubr::theme_pubclean() +
    theme(axis.ticks.x=element_blank(), axis.text.x=element_blank())

plt.glmer.swab$data$facet <- rep(
    c("Intercept | Month", "Healthcare Worker | Month"),
    each=levels(sub.tbl$month) %>% length()
)

ggsave("fig/glmer-swab.pdf", plot=plt.glmer.swab, width=10, height=6)

plt.glmer.death <- plot_model(
    glmer.death, type="re", facet.grid=TRUE, axis.lim=c(0.5, 2), ci.lvl=0.95,
    show.p=TRUE, show.values=TRUE, show.data=TRUE, dot.size=2.5, line.size=1,
    value.size=4, p.val="wald", colors=c("grey60", "grey20"),
    vline.color="grey70"
) +
    labs(
        title    = "Monthly variance of the odds ratio of mortality",
        subtitle = "Month of data collection as a random effect"
    ) +
    ggpubr::theme_pubclean() +
    theme(axis.ticks.x=element_blank(), axis.text.x=element_blank())

plt.glmer.death$data$facet <- rep(
    c("Intercept | Month", "Healthcare Worker | Month"),
    each=levels(sub.tbl$month) %>% length()
)

ggsave("fig/glmer-death.pdf", plot=plt.glmer.death, width=10, height=6)

ggpubr::ggarrange(plt.glmer.swab, plt.glmer.death, ncol=2)
ggsave("fig/glmer-outcome.pdf", height=6, width=15)

# Adjusting the variables for plotting
sub.tbl %<>% inset(c("HCW", "pred.swab", "pred.death"), value=list(
    "HCW"        = factor(.$hcn, levels=c(1, 0), labels=c("Yes", "No")), # Set variable grouping
    "pred.swab"  = predict(glmer.swab), # Predicting the logit
    "pred.death" = predict(glmer.death)
)) %>% inset(c("prob.swab", "prob.death"), value=list(
    "prob.swab"  = exp(.$pred.swab)  / {1 + exp(.$pred.swab)}, # Calculating the likelihood
    "prob.death" = exp(.$pred.death) / {1 + exp(.$pred.death)}
))

# Aggregate the table for easier plotting
tbl.plt <- aggregate(
    cbind(prob.swab, prob.death) ~ HCW + month, data=sub.tbl, mean
)

# Plotting the likelihood of positive swab results
nudge.val <- 1e-1 * c(1, -1)

plt.prob.swab <- ggplot(tbl.plt, aes(x=month, fill=HCW, color=HCW)) +
    geom_point(aes(y=prob.swab), size=2.5) +
    annotate("rect", xmin=0.5, xmax=8.5, ymin=-5e-2, ymax=max(tbl.plt$prob.death) + 5e-2, fill="grey90") +
    annotate(
        "segment", x=0.5, xend=8, y=1.2, yend=1.2, color="grey90", size=20, linejoin="mitre",
        arrow=arrow(length=unit(1, "mm"), type="closed")
    ) +
    geom_point(aes(y=prob.death), size=2.5*0.5) +
    geom_text(aes(y=prob.swab, label=scales::percent(prob.swab)), nudge_y=nudge.val, size=3) +
    scale_y_continuous(aes(y=prob.swab), labels=scales::percent, limits=c(-5e-2, 1.32)) +
    geom_path(aes(y=prob.swab, group=HCW),  linetype=3, size=1, alpha=0.4) +
    geom_path(aes(y=prob.death, group=HCW), linetype=3, size=1*0.5, alpha=0.4) +
    scale_color_manual(values=c("Yes"="grey20", "No"="grey60")) +
    labs(
        title    = "How do we fare against COVID-19?",
        subtitle = "Likelihood of being infected in each month",
        fill     = "Working in the heathcare sector?",
        color    = "Working in the heathcare sector?"
    ) +
    annotate(
        "text",  x=0.5, y=max(tbl.plt$prob.death) + 5e-2,
        label="The likelihood of death, plotted on the same scale as the likelihood of being infected",
        hjust=0, vjust=-1, size=3
    ) +
    annotate("label", x=1,   y=1.2, size=3, fill="grey90", label.size=0, label="Ban mass\ngathering") +
    annotate("label", x=2,   y=1.2, size=3, fill="grey90", label.size=0, label="LSSR &\nSocial assistance") +
    annotate("label", x=3,   y=1.2, size=3, fill="grey90", label.size=0, label="JDCN & Ban\nmass exodus") +
    annotate("label", x=4,   y=1.2, size=3, fill="grey90", label.size=0, label="LSSR transition &\nEnforce workplace\nCOVID-19 protocol") +
    annotate("label", x=5.1, y=1.2, size=3, fill="grey90", label.size=0, label="COVID-19\ncampaign &\nEvaluate LSSR") +
    annotate("label", x=6.3, y=1.2, size=3, fill="grey90", label.size=0, label="Integrate hospital\nbed management") +
    annotate("label", x=7.3, y=1.2, size=3, fill="grey90", label.size=0, label="Social\nsecurity\npolicy") +
    ggpubr::theme_pubclean() +
    theme(
        legend.position      = c(0.9, 1.06),
        legend.direction     = "horizontal",
        legend.justification = "right",
        axis.ticks           = element_blank(),
        axis.title           = element_blank(),
        axis.text            = element_blank(),
        panel.grid.major.y   = element_blank()
    )

plt.prob.death <- ggplot(tbl.plt, aes(x=month, y=prob.death, fill=HCW, color=HCW)) +
    geom_point(size=2.5) +
    geom_text(aes(label=scales::percent(prob.death %>% round(4))), nudge_y=-2e-1 * nudge.val, size=3) +
    scale_y_continuous(aes(y=prob.swab), labels=scales::percent) +
    geom_path(aes(group=HCW), linetype=3, size=1, alpha=0.4) +
    scale_color_manual(values=c("Yes"="grey20", "No"="grey60")) +
    labs(
        subtitle = "Likelihood of death in each month (scaled up)",
        #caption = "Healthcare workers are more likely to be infected",
        x = "DKI Jakarta progress in 2020"
    ) +
    annotate(
        "segment", x=4, xend=4, y=max(tbl.plt$prob.death) + 3.5e-2,
        yend=max(tbl.plt$prob.death) + 4.5e-2, color="grey40"
    ) +
    annotate(
        "text", x=4, y=max(tbl.plt$prob.death) + 3e-2,
        label="Diminishing likelihood of death, especially from June onwards",
        size=3, vjust=-1.3, hjust=1.01
    ) +
    annotate(
        "segment", x=4, xend=8, y=max(tbl.plt$prob.death) + 4e-2,
        yend=max(tbl.plt$prob.death) + 4e-2, color="grey40",
        arrow=arrow(length=unit(2, "mm"), type="closed")
    ) +
    ggpubr::theme_pubclean() +
    theme(
        axis.ticks         = element_blank(),
        axis.title.y       = element_blank(),
        axis.text.y        = element_blank(),
        panel.grid         = element_blank(),
        legend.position    = "none",
        panel.background   = element_rect(fill="grey90"),
        panel.grid.major.y = element_blank()
    )

ggpubr::ggarrange(
    plt.prob.swab, plt.prob.death,
    ncol=1, heights=c(6, 3)
)
ggsave("fig/prob-outcome.pdf", height=8, width=10)
