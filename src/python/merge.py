#!/bin/python3

## LOAD MODULES

import os
import re
import datetime
import pandas as pd
import numpy as np


## CONFIG

pd.options.display.max_columns = None


## FUNC

def parse_excel(filepath, **kwargs):
    """
    Read an excel file based on its extension
    """
    try:
        tbl = pd.read_excel(filepath, **kwargs)
    except OSError: # Happen when parsing *.xlsb
        tbl = pd.read_excel(filepath, engine="pyxlsb", **kwargs)
    return tbl


## PREP

# List datasets
path_eis = "data/raw//DATA EIS COVID-19/"
path_pos = "data/raw/MERGING/POSITIF/"
path_neg = "data/raw/MERGING/NEGATIF/"

file_eis = [ # EIS data
    os.path.abspath(path_eis + filename) for filename in os.listdir(path_eis)
]

file_pos = [ # Positive lab results (confirmed cases)
    os.path.abspath(path_pos + filename) for filename in os.listdir(path_pos)
]

file_neg = [ # Negative lab results
    os.path.abspath(path_neg + filename) for filename in os.listdir(path_neg)
]

# Column to parse
col_eis = ["A:BI,CH:CQ"] * len(file_eis)
col_pos = ["B:BJ,BU"]    * len(file_pos)
col_neg = ["B:P"]        * len(file_neg)

# Set header level for column indicator
hdr_eis = [0] * len(file_eis)
hdr_pos = [0] * len(file_pos)
hdr_neg = [3, 0, 1, 0]

# Set colnames for each data
name_eis = [
    "name", "NIK", "age", "date_birth", "birth_place", "sex", "job", "marital",
    "address_NIK", "address", "RT", "RW", "kelurahan", "kecamatan", "district",
    "HP", "nationality", "country", "reporting_facility", "date_onset",
    "sym_fever" "sym_temperature", "sym_cough", "sym_coryza",
    "sym_sore_throat", "sym_difficulty_breathing", "sym_shiver",
    "sym_headache", "sym_malaise", "sym_myalgia", "sym_nausea", "sym_abd_pain",
    "sym_diarrhea", "sym_other", "pneumonia", "com_pregnant", "com_diabetes",
    "com_CHD", "com_HT", "com_malignancy", "com_immune_disorder", "com_CRF",
    "com_CLD", "com_COPD", "other_comorbid", "case_status", "care_ICU",
    "care_intub", "care_EMCO", "refer_hospital", "travel_history",
    "date_travel", "date_arrival", "close_contact_history", "contact_name",
    "wet_market", "facility_visit_history", "facility", "HC_worker",
    "specimen_collect", "date_specimen_collect", "lab_name", "xray",
    "xray_result", "leukocyte_test", "leukocyte", "lymphocyte", "thrombocyte",
    "laborant", "date_lab_exam", "info", "date_specimen_result"
]

name_pos = [
    "name", "NIK", "ID", "close_contact", "date_birth", "age", "sex", "job",
    "address", "RT", "RW", "info", "kelurahan", "kecamatan", "district", "HP",
    "nationality", "active_tracing", "reporting_facility", "date_onset",
    "sym_fever", "sym_temperature", "sym_cough", "sym_coryza",
    "sym_sore_throat", "sym_difficulty_breathing", "sym_shiver",
    "sym_headache", "sym_malaise", "sym_myalgia", "sym_nausea", "sym_abd_pain",
    "sym_diarrhea", "sym_pneumonia", "com_pregnant", "com_diabetes", "com_CHD",
    "com_HT", "com_malignancy", "com_immune_disorder", "com_CRF", "com_CLD",
    "com_COPD", "com_obesity", "care_ICU", "care_intub", "care_EMCO",
    "travel_history", "date_travel", "date_arrival", "close_contact_history",
    "wet_market", "facility_visit_history", "facility", "HC_worker",
    "date_close_contact", "collect_specimen", "date_specimen_collect",
    "lab_name", "lab_result", "date_specimen_result", "status"
]

name_neg = [
    "NIK", "name", "age", "date_birth", "sex", "reporting_facility",
    "lab_result", "lab_name", "date_specimen_receive", "date_specimen_result",
    "collect_specimen", "address", "info", "date_swab", "date_exam"
]

# Read file as a list of workbook
tbl_eis = [
    parse_excel(filepath, header=hdr, usecols=col, names=name_eis)
        for filepath, hdr, col in zip(file_eis, hdr_eis, col_eis)
]

tbl_pos = [
    parse_excel(filepath, header=hdr, usecols=col, names=name_pos)
        for filepath, hdr, col in zip(file_pos, hdr_pos, col_pos)
]

tbl_neg = [
    parse_excel(filepath, header=hdr, usecols=col, names=name_neg)
        for filepath, hdr, col in zip(file_neg, hdr_neg, col_neg)
]

# Sanity check on column name consistency, then concatenate all entries
if all([all(tbl.columns == tbl_eis[0].columns) for tbl in tbl_eis]):
    tbl_eis = pd.concat(tbl_eis)

if all([all(tbl.columns == tbl_pos[0].columns) for tbl in tbl_pos]):
    tbl_pos = pd.concat(tbl_pos)

if all([all(tbl.columns == tbl_neg[0].columns) for tbl in tbl_neg]):
    tbl_neg = pd.concat(tbl_neg)

# Create identifier for each sources
tbl_eis["source"] = "EIS"
tbl_pos["source"] = "Positive"
tbl_neg["source"] = "Negative"


## MERGE

# Combine into one large data frame (read the trade-off: `combine_first`)
data_merge = tbl_eis.combine_first(tbl_pos).combine_first(tbl_neg).reset_index(
    drop = True
)

# Remove temporary tables to preserve memory
del tbl_eis; del tbl_pos; del tbl_neg


## CLEAN

# Get the column names
colnames   = data_merge.columns

# Remove entries which all columns are NA
data_merge.dropna(axis=0, how="all", inplace=True)

# Set reference fields to find duplicates
ref_col    = [
    "name", "date_birth", "date_specimen_result", "NIK", "age", "address",
    "lab_result"
]

data_nodup = data_merge.drop_duplicates(ref_col)

# Fill in the job field using name
data_nodup.job = np.where(
    data_nodup.name.str.contains("dr\.\s", regex=True, case=False),
    "dokter", np.where(
        data_nodup.name.str.contains("drg\.\s", regex=True, case=False),
        "dokter gigi", np.where(
            data_nodup.name.str.contains("(nz|ners|nz|zr)\.?\s", regex=True, case=False),
            "perawat", data_nodup.job
        )
    )
)

# Regularize the job field using lower case letters
data_nodup.job = data_nodup.job.str.lower()

# Regularize the job naming for nurses
data_nodup.job[
    data_nodup.job.str.contains("(ners|nz|zr|ns).?\s", regex=True, na=False)
] = "perawat"

# Clean HC_worker field
data_nodup.HC_worker[
    data_nodup.HC_worker.apply(lambda x: isinstance(x, datetime.datetime))
] = np.NaN

data_nodup.HC_worker.replace("(0.*|BUKAN|t|-)", 0, regex=True, inplace=True)
data_nodup.HC_worker.replace("(Klin|RS|SEHAT)", 1, regex=True, inplace=True)
data_nodup.HC_worker[data_nodup.HC_worker == 9] = 0

# Correct HC_worker field based on job
data_nodup.HC_worker[
    (data_nodup.job.str.contains(
        "(dokter|perawat|bidan)", regex=True, case=False, na=False
    ))
] = 1

# Clean the name field
data_nodup.name = data_nodup.name.str.upper().replace(
    "(^(\w{2,})\W*\s+|(,\s*(\w\.?){2,}\s*))", "", regex=True
)

# Clean the NIK field
data_nodup.NIK = data_nodup.NIK \
    .replace("(\W|KTP|PASPOR|nan)", "", regex=True) \
    .replace("^$", np.NaN, regex=True).astype("string")

data_nodup["NIK_clean"] = data_nodup.NIK[
    (data_nodup.NIK.str.len() == 16) &
    (data_nodup.NIK.str.contains("^\d+$", regex=True, na=False))
].astype("string")

# Obtaining passport
data_nodup["passport"]  = data_nodup.NIK[
    data_nodup.NIK.str.contains("^[A-Za-z]+\d+$", regex=True, na=False)
]

# Separating other identifiers
data_nodup["other_identifier"] = data_nodup.NIK[
    (data_nodup.NIK_clean.isnull()) & (data_nodup.passport.isnull())
]

# Obtain province and district from `NIK_clean`
index = ~data_nodup.NIK_clean.isnull()
data_nodup[["province", "district2"]] = np.nan

data_nodup["province"][index] = data_nodup.NIK_clean[index] \
    .apply(lambda x: x[:2])

data_nodup["district2"][index] = data_nodup.NIK_clean[index] \
    .apply(lambda x: x[2:4])

# Get all column names containing date-like values
dates = colnames[[
    True if pattern != None else False for pattern in [
        re.match("^date_.*", col) for col in colnames
    ]
]]

# Set index to subset empty DoB
index = data_nodup.date_birth.isnull() & \
    ~data_nodup.NIK_clean.isnull() & \
    data_nodup.NIK_clean.apply( # Only includes valid months in NIK entry
        lambda x: int(x[8:10])<=12 if type(x)==str else False
    )

# Extract DoB to evaluate date validity
dob_extract = data_nodup.NIK_clean[index].apply(
    lambda x: x[6:12] if int(x[6:8])<=31 else str(int(int(x[6:12]) - 4e5))
)

# List invalid DoB
dob_invalid = []
for i, v in enumerate(dob_extract):
    try:
        datetime.datetime.strptime(v, "%d%m%y")
    except ValueError:
        dob_invalid.append(data_nodup.index[index][i])

# Re-indexing to exclude invalid entries
index = index & ~data_nodup.index.isin(dob_invalid)

# Imputing empty date of birth from `NIK_clean`
data_nodup.date_birth[index] = data_nodup.NIK_clean[index].apply(
    lambda x: x[6:12] if int(x[6:8])<=31 else str(int(int(x[6:12]) - 4e5))
).apply(lambda x: datetime.datetime.strptime(x, "%d%m%y") if float(x)>0 else np.nan)

# Create uniform status as outcome
index = ~data_nodup.status.isnull()
data_nodup[index].status = data_nodup[index].status.apply(lambda x: x.lower())

# List comorbid columns
comorbids = colnames[[
    True if pattern != None else False for pattern in [
        re.match("^com_.*", col) for col in colnames
    ]
]]

# Clean the comorbid data into True and False
data_nodup[comorbids] = data_nodup[comorbids] \
    .apply(lambda x: x.replace(".*(tidak|nihil).*(?i)", 0, regex=True), axis=0)

data_nodup[comorbids] = data_nodup[comorbids] \
    .apply(lambda x: x.replace(".*ya.*(?i)", 0, regex=True), axis=0)

# Specific cleaning on selected comorbidities
data_nodup.com_immune_disorder = data_nodup.com_immune_disorder \
    .replace("1?.*\w+", 1, regex=True)

data_nodup.com_pregnant = data_nodup.com_pregnant \
    .replace("hamil", 1, regex=True)

data_nodup.com_obesity = data_nodup.com_obesity \
    .replace("\n", np.nan, regex=True)

data_nodup[comorbids].apply(lambda x: x.unique(), axis=0)

# Set comorbidities as boolean data type
data_nodup[comorbids] = data_nodup[comorbids].astype("boolean")


## SPLIT

# Create two dataset to write as excel files
n = int(5e5)
nodup1 = data_nodup.iloc[:n, :]
nodup2 = data_nodup.iloc[n:, :]


# WRITE

# Save as csv
data_merge.to_csv("data/processed/data-merge.csv", index=False)
data_nodup.to_csv("data/processed/data-nodup.csv", index=False)
nodup1.to_csv("data/processed/data-nodup1.csv", index=False)
nodup2.to_csv("data/processed/data-nodup2.csv", index=False)

# District and source identifier
colnames = ["district", "source"]
nodup1[colnames].to_csv("_processed/identifier-nodup1.csv", index=False)
nodup2[colnames].to_csv("_processed/identifier-nodup2.csv", index=False)
