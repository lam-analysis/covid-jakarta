#!/bin/python3

## LOAD MODULES

from openpyxl import load_workbook


## FUNC

def get_colnames(ws, **kwarg):
    """
    Get column names of a worksheet
    """
    return {
        col : ws.cell(column=col, **kwarg).value
            for col in range(1, ws.max_column + 1)
    }


## PREP

# Set excel file to read
fname = "data/raw/MERGING/NEGATIF/1. Hasil Lab Rujukan s.d. 27.08 16.00 WIB.xlsx"
ref   = "data/raw/MERGING/NEGATIF/3. DATA 15 SEPTEMBER - 30 SEPTEMBER 2020.xlsx"

# Read workbook
wb     = load_workbook(fname)
wb_ref = load_workbook(ref)

# Get the worksheet
ws     = wb.get_sheet_by_name(wb.sheetnames[0])
ws_ref = wb_ref.get_sheet_by_name(wb_ref.sheetnames[0])

# Manually examine column names
colnames     = get_colnames(ws, row=3)
colnames_ref = get_colnames(ws_ref, row=1)


## CLEAN

# Insert missing columns
ws.insert_cols(5)
ws.insert_cols(12, 2)
ws.insert_cols(16)

# Delete unnecessary columns
del_col = 19
ws.delete_cols(del_col, ws.max_column - del_col)


## WRITE

# Replace the worksheet with the regularized table
wb.save(fname)
