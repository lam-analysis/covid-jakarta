*Define variable*
hcn1 (1. nakes, 0.non nakes)
age_edit if age>=18 (dipakai >=18)
sex2 (0. Laki-laki, 1. Perempuan)
lokasi (0. Non DKI, 1. DKI)
kontak (0. Tidak, 1.Ya)
death if resultsps ==1 (status kematian ketika hasil swab positif)
resultswab (0. Negatif; 1. Positif)
resultsps (bulan status) Feb sd Okt 2020
allPSBB (periode by kebijakan berdasarkan linimasa dari Dinkes DKI)
*Symptoms*
cough  (0. Tidak, 1.Ya)
diare (0. Tidak, 1.Ya)
difbreath (0. Tidak, 1.Ya)
fever (0. Tidak, 1.Ya)
headache (0. Tidak, 1.Ya)
malaise (0. Tidak, 1.Ya)
myalgia (0. Tidak, 1.Ya)
nausea (0. Tidak, 1.Ya)
soreth (0. Tidak, 1.Ya)
abdp (0. Tidak, 1.Ya)
*Comorbid*
ht_com  (0. Tidak, 1.Ya)
dm_com  (0. Tidak, 1.Ya)
PPOK (0. Tidak, 1.Ya)
Liver  (0. Tidak, 1.Ya)
asma (0. Tidak, 1.Ya) 
npneumoni  (0. Tidak, 1.Ya)

*Univariate*
tab hcn1 if age >=18
sum age  if age>=18
tab sex if age>=18
tab lokasi if age>=18
tab coug  if age>=18
tab diare  if age>=18
tab difb  if age>=18
tab fever  if age>=18
tab head  if age>=18
tab malai  if age>=18
tab myal  if age>=18
tab naus  if age>=18
tab soreth  if age>=18
tab abdp  if age>=18
tab ht  if age>=18
tab dm  if age>=18
tab PP  if age>=18
tab Liv  if age>=18
tab asm  if age>=18
tab npneum  if age>=18
tab konta  if age>=18
tab death1 if age>=18
tab resultsps if age>=18
*Bivariate*
tab hcn1 death if age>=18 & resultswab==1, row col
sort death
by death: sum age if age>=18 & resultswab==1
tab sex death if age>=18 & resultswab==1, row col
tab lokasi death if age>=18 & resultswab==1, row col
tab coug death if age>=18 & resultswab==1, row col
tab fever death if age>=18 & resultswab==1, row col
tab diare death if age>=18 & resultswab==1, row col
tab head death if age>=18 & resultswab==1, row col
tab malais death if age>=18 & resultswab==1, row col
tab myalgia death if age>=18 & resultswab==1, row col
tab nausea death if age>=18 & resultswab==1, row col
tab difbreath death if age>=18 & resultswab==1, row col
tab soreth death if age>=18 & resultswab==1, row col
tab ht_com death if age>=18 & resultswab==1, row col
tab dm_com death if age>=18 & resultswab==1, row col
tab PPOK death if age>=18 & resultswab==1, row col
tab Liv death if age>=18 & resultswab==1, row col
tab asm death  if age>=18 & resultswab==1, row col
tab npneum death if age>=18 & resultswab==1, row col
tab abdp death if age>=18 & resultswab==1, row col
tab kontak death if age>=18 & resultswab==1, row col


logit death hcn1 if age>=18 & resultswab==1, or
logit death age if age>=18 & resultswab==1, or
logit death sex if age>=18 & resultswab==1, or
logit death lokasi if age>=18 & resultswab==1, or
logit death coug if age>=18 & resultswab==1, or
logit death fever if age>=18 & resultswab==1, or
logit death diare if age>=18 & resultswab==1, or
logit death head if age>=18 & resultswab==1, or
logit death malais if age>=18 & resultswab==1, or
logit death myalgia if age>=18 & resultswab==1, or
logit death nausea if age>=18 & resultswab==1, or
logit death difbreath if age>=18 & resultswab==1, or
logit death soreth if age>=18 & resultswab==1, or
logit death ht_com if age>=18 & resultswab==1, or
logit death dm_com if age>=18 & resultswab==1, or
logit death PPOK if age>=18 & resultswab==1, or
logit death Liv if age>=18 & resultswab==1, or
logit death asm  if age>=18 & resultswab==1, or
logit death npneum if age>=18 & resultswab==1, or
logit death abdp if age>=18 & resultswab==1, or
logit death kontak if age>=18 & resultswab==1, or


*Adjustement (Multivariate)*
tab hcn1 if filter_manuskrip==1
tab sex if filter_manuskrip==1
tab lokasi if filter_manuskrip==1
tab fever if filter_manuskrip==1
tab head if filter_manuskrip==1
tab malais if filter_manuskrip==1
tab myalgia if filter_manuskrip==1
tab naus if filter_manuskrip==1
tab difb if filter_manuskrip==1
tab ht if filter_manuskrip==1
tab dm if filter_manuskrip==1
tab PPOK if filter_manuskrip==1
tab Liver if filter_manuskrip==1
tab kontak if filter_manuskrip==1
sort death
by  death: sum  age if filter_manuskrip==1

*OR by Month 
*Covid cases
sort resultsps
by resultsps: logit resultswab hcn1 if age>=18,or

*Death cases
sort resultsps
by resultsps: logit death hcn1 if age>=18 & resultswab==1 ,or
