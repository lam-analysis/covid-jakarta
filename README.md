# Background

This is a collaborative analysis between Medical Technology Cluster and Human Nutrition Research Cluster, IMERI - FKUI. The data required in this analysis *is not* in the public domain, so you will have to make a formal request to the institution.

# Getting started

The recommended procedure of reproducing this analysis is by using the command line. You will need to clone the repository then run all scripts from the root directory. However, if you are keen on having a more beginner-friendly approach, with as little familiarity with the command line, you may want to consider using RStudio. In RStudio, you will have to initiate a new R project, then choose from git repository.

After you set up your project by cloning manually or through RStudio, you will then need to acquire the data from our collaborators. To start analyzing and reporting, you will only concern with the content of `src/R` directory, i.e.:

```
src
├── python
│   └── . . .
├── R
│   ├── 01-convert.R
│   ├── 02-glmer.R
│   ├── 03-brms.R
│   └── 04-dataviz.R
└── stata
    └── . . .
```
 
Then, you will need to put `data.dta` file (a `stata` data frame) into `data/processed` directory, i.e.:

```
data
├── processed
│   ├── . . .
│   ├── data.dta
│   └── . . .
└── raw
    └── . . .
```

Finally, you can start running the script by sourcing the `analysis.R` file through your R session. Alternatively, you can also sequentially run the content of `src/R` directory, from `01-convert.R` to `04-dataviz.R`.
